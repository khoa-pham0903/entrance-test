//
//  BaseTableViewCell.swift
//  Entrance Test
//
//  Created by Khoa Pham on 5/17/19.
//  Copyright © 2019 Khoa Pham. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell, CellInterface {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupView() {
        selectionStyle = .none
    }
}
