//
//  AlertDisplayer.swift
//  Entrance Test
//
//  Created by Khoa Pham on 5/16/19.
//  Copyright © 2019 Khoa Pham. All rights reserved.
//

import Foundation
import UIKit

protocol AlertDisplayer {
    func displayAlert(with title: String?, message: String?, style: UIAlertController.Style, actions: [UIAlertAction]?)
}

extension AlertDisplayer where Self: UIViewController {
    func displayAlert(with title: String?, message: String?, style: UIAlertController.Style, actions: [UIAlertAction]? = nil) {
        guard presentedViewController == nil else {
            return
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        actions?.forEach { action in
            alertController.addAction(action)
        }
        present(alertController, animated: true)
    }
}
