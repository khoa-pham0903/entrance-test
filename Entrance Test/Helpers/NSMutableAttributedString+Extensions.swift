//
//  NSMutableAttributedString+Extensions.swift
//  Entrance Test
//
//  Created by Khoa Pham on 5/16/19.
//  Copyright © 2019 Khoa Pham. All rights reserved.
//

import Foundation
import UIKit

extension NSMutableAttributedString {
    func addString(_ text: String, font: UIFont, color: UIColor) -> NSMutableAttributedString {
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .foregroundColor: color,
        ]
        let attributedString = NSMutableAttributedString(string:text, attributes: attributes)
        append(attributedString)
        return self
    }
}
