//
//  UIView+Extensions.swift
//  Entrance Test
//
//  Created by Khoa Pham on 5/17/19.
//  Copyright © 2019 Khoa Pham. All rights reserved.
//

import UIKit

extension UIView {
    static func loadNib<T: UIView>(_ viewType: T.Type) -> T {
        return Bundle(for: viewType).loadNibNamed(String(describing: viewType), owner: nil, options: nil)!.first as! T
    }
    
    static func loadNib() -> Self {
        return loadNib(self)
    }
}
