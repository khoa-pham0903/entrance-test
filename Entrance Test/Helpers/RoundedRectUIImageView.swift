//
//  RoundedRectUIImageView.swift
//  Entrance Test
//
//  Created by Khoa Pham on 5/16/19.
//  Copyright © 2019 Khoa Pham. All rights reserved.
//

import UIKit

@IBDesignable class RoundedRectUIImageView: UIImageView {

    @IBInspectable var cornerRadius: CGFloat {
        set
        {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get
        {
            return layer.cornerRadius
        }
    }

}
