//
//  FoodDataProvider.swift
//  Entrance Test
//
//  Created by Khoa Pham on 5/16/19.
//  Copyright © 2019 Khoa Pham. All rights reserved.
//

import Foundation

struct FoodDataProvider {
    
    static func generateVegetarianFoods() -> [Food] {
        let food1 = Food(id: 1, name: "Avocado Salad", tags: "Healthy", delivery: "Order from $12 • 10 min delivery", photo: "1", hasVideo: true)
        let food2 = Food(id: 2, name: "Cashew Chicken", tags: "Healthy", delivery: "Order from $12 • 20 min delivery", photo: "2", hasVideo: false)
        let food3 = Food(id: 3, name: "Cilantro Lime Shrimp", tags: "Healthy", delivery: "Order from $12 • 30 min delivery", photo: "3", hasVideo: true)
        let food4 = Food(id: 4, name: "Gingery Pork", tags: "Healthy", delivery: "Order from $12 • 40 min delivery", photo: "4", hasVideo: false)
        let food5 = Food(id: 5, name: "Grilled Steak Salad", tags: "Healthy", delivery: "Order from $12 • 50 min delivery", photo: "5", hasVideo: true)
        let food6 = Food(id: 6, name: "Oven Roasted Vegetables", tags: "Healthy", delivery: "Order from $12 • 60 min delivery", photo: "6", hasVideo: false)
        let food7 = Food(id: 7, name: "Sesame Chicken", tags: "Healthy", delivery: "Order from $12 • 70 min delivery", photo: "7", hasVideo: true)
        let food8 = Food(id: 8, name: "Shrimp Sandwich", tags: "Healthy", delivery: "Order from $12 • 80 min delivery", photo: "8", hasVideo: false)
        let food9 = Food(id: 9, name: "Stir-Fry Chicken", tags: "Healthy", delivery: "Order from $12 • 90 min delivery", photo: "9", hasVideo: true)
        let food10 = Food(id: 10, name: "Yogurt Breakfast", tags: "Healthy", delivery: "Order from $12 • 100 min delivery", photo: "10", hasVideo: false)
        return [food1, food2, food3, food4, food5, food6, food7, food8, food9, food10]
    }
    
    static func generateHealthyFoods() -> [Food] {
        let food1 = Food(id: 11, name: "Bean Stew", tags: "Vegetarian", delivery: "Order from $12 • 10 min delivery", photo: "11", hasVideo: false)
        let food2 = Food(id: 12, name: "Eggplant Lasagna", tags: "Vegetarian", delivery: "Order from $12 • 20 min delivery", photo: "12", hasVideo: true)
        let food3 = Food(id: 13, name: "Green Curry", tags: "Vegetarian", delivery: "Order from $12 • 30 min delivery", photo: "13", hasVideo: false)
        let food4 = Food(id: 14, name: "Grilled Vegatable", tags: "Vegetarian", delivery: "Order from $12 • 40 min delivery", photo: "14", hasVideo: true)
        let food5 = Food(id: 15, name: "Pumpkin and Peanut", tags: "Vegetarian", delivery: "Order from $12 • 50 min delivery", photo: "15", hasVideo: false)
        let food6 = Food(id: 16, name: "Spicy Zucchini Soup", tags: "Vegetarian", delivery: "Order from $12 • 60 min delivery", photo: "16", hasVideo: true)
        let food7 = Food(id: 17, name: "Stir Fried Millet", tags: "Vegetarian", delivery: "Order from $12 • 70 min delivery", photo: "17", hasVideo: false)
        let food8 = Food(id: 18, name: "Vegetarian Chili", tags: "Vegetarian", delivery: "Order from $12 • 80 min delivery", photo: "18", hasVideo: true)
        let food9 = Food(id: 19, name: "Vegetarian Pizzas", tags: "Vegetarian", delivery: "Order from $12 • 90 min delivery", photo: "19", hasVideo: false)
        let food10 = Food(id: 20, name: "Vegetarian Tacos", tags: "Vegetarian", delivery: "Order from $12 • 100 min delivery", photo: "20", hasVideo: true)
        return [food1, food2, food3, food4, food5, food6, food7, food8, food9, food10]
    }
    
    static func generateAllFoods() -> [Food] {
        var foods = generateVegetarianFoods()
        foods.append(contentsOf: generateHealthyFoods())
        return foods
    }
}
