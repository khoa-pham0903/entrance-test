//
//  Food.swift
//  Entrance Test
//
//  Created by Khoa Pham on 5/17/19.
//  Copyright © 2019 Khoa Pham. All rights reserved.
//

import Foundation

struct Food {
    let id: Int
    let name: String
    let tags: String
    let delivery: String
    let photo: String
    let hasVideo: Bool
}
