//
//  FoodDetails.swift
//  Entrance Test
//
//  Created by Khoa Pham on 5/17/19.
//  Copyright © 2019 Khoa Pham. All rights reserved.
//

import Foundation

class FoodDetails {
    let name: String
    let photo: String
    var quantity: Int
    let extras: [String]
    var selectedExtras: String
    
    init(name: String, photo: String, quantity: Int, extras: [String], selectedExtras: String) {
        self.name = name
        self.photo = photo
        self.quantity = quantity
        self.extras = extras
        self.selectedExtras = selectedExtras
    }
    
    convenience init(name: String, photo: String) {
        self.init(name: name, photo: photo, quantity: 1, extras: ["Add jumbo shrimp",
                                                                  "Add cheese",
                                                                  "Add white anchovies"], selectedExtras: "")
    }
}
