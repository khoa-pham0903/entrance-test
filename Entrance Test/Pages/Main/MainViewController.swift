//
//  MainViewController.swift
//  Entrance Test
//
//  Created by Khoa Pham on 5/15/19.
//  Copyright © 2019 Khoa Pham. All rights reserved.
//

import UIKit

private let animationDuration: TimeInterval = 0.3

private let listLayoutStaticCellHeight: CGFloat = 110
private let gridLayoutStaticCellHeight: CGFloat = 260

class MainViewController: UIViewController, AlertDisplayer {
    
    enum FoodCategories: String {
        
        case vegetarian = "Vegetarian"
        case healthy = "Healthy"
        
        var currentFoods: [Food] {
            switch self {
            case .vegetarian:
                return FoodDataProvider.generateVegetarianFoods()
            case .healthy:
                return FoodDataProvider.generateHealthyFoods()
            }
        }
    }

    @IBOutlet weak var collectionView: UICollectionView!
    var isTransitionAvailable = true
    lazy var listLayout = DisplaySwitchLayout(staticCellHeight: listLayoutStaticCellHeight, nextLayoutStaticCellHeight: gridLayoutStaticCellHeight, layoutState: .list)
    lazy var gridLayout = DisplaySwitchLayout(staticCellHeight: gridLayoutStaticCellHeight, nextLayoutStaticCellHeight: listLayoutStaticCellHeight, layoutState: .grid)
    var layoutState: LayoutState = .list
    
    lazy var titleButton: UIButton = {
        let titleButton = UIButton(type: .system)
        titleButton.frame = CGRect(x: 0, y: 0, width: 200, height: 50)
        titleButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        titleButton.setImage(imageFromName("icon_drop_down"), for: .normal)
        titleButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 150, bottom: 0, right: 0)
        titleButton.addTarget(self, action: #selector(titleButtonTapped), for: .touchUpInside)
        return titleButton
    }()
    
    var categories: FoodCategories = .vegetarian {
        didSet {
            collectionView.reloadData()
        }
    }
    
    let searchController = UISearchController(searchResultsController: nil)
    var searchVC: SearchViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setupCollectionView()
        initData()
    }
    
    func setupNavBar() {
        navigationController?.navigationBar.isTranslucent = false
        let listButton = UIBarButtonItem(image: imageFromName("list_icon"), style: .plain, target: self, action: #selector(listButtonTapped))
        let searchButton = UIBarButtonItem(image: imageFromName("icon_search"), style: .plain, target: self, action: #selector(searchButtonTapped))
        navigationItem.rightBarButtonItems = [listButton, searchButton]
        navigationItem.titleView = titleButton
        navigationItem.searchController = nil
        updateUI()
    }
    
    private func initData() {
        categories = .vegetarian
    }
    
    private func updateUI() {
        titleButton.setTitle(categories.rawValue, for: .normal)
    }
    
    @objc
    func titleButtonTapped() {
        let cancelAction = UIAlertAction(title: "Huỷ bỏ", style: .cancel, handler: nil)
        let vegetarianAction = UIAlertAction(title: "Vegetarian", style: .default, handler: { (action) in
            self.categories = .vegetarian
            self.updateUI()
        })
        let healthyAction = UIAlertAction(title: "Healthy", style: .default, handler: { (action) in
            self.categories = .healthy
            self.updateUI()
        })
        displayAlert(with: nil, message: nil, style: .actionSheet, actions: [cancelAction, vegetarianAction, healthyAction])
    }
    
    @objc
    func listButtonTapped() {
        if !isTransitionAvailable {
            return
        }
        let transitionManager: TransitionManager
        if layoutState == .list {
            layoutState = .grid
            transitionManager = TransitionManager(duration: animationDuration, collectionView: collectionView!, destinationLayout: gridLayout, layoutState: layoutState)
        } else {
            layoutState = .list
            transitionManager = TransitionManager(duration: animationDuration, collectionView: collectionView!, destinationLayout: listLayout, layoutState: layoutState)
        }
        transitionManager.startInteractiveTransition()
    }

    func setupSearchBar() {
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.autocapitalizationType = .none
        searchController.searchBar.autocorrectionType = .no
        searchController.searchBar.placeholder = "Search Foods"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        searchController.searchBar.becomeFirstResponder()
    }
    
    @objc
    func searchButtonTapped() {
        resetNavBar()
        setupSearchBar()
        openSearchScreen()
    }
    
    func openSearchScreen() {
        searchVC = SearchViewController(searchController: searchController)
        searchVC.delegate = self
        addChild(searchVC)
        view.bounds = searchVC.view.frame
        view.addSubview(searchVC.view)
        searchVC.didMove(toParent: self)
    }
    
    func closeSearchScreen() {
        searchVC.willMove(toParent: nil)
        searchVC.view.removeFromSuperview()
        searchVC.removeFromParent()
    }
    
    func resetNavBar() {
        navigationItem.rightBarButtonItems = nil
        navigationItem.titleView = nil
    }
}


