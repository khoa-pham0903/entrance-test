//
//  MainTabBarViewController.swift
//  Entrance Test
//
//  Created by Khoa Pham on 5/15/19.
//  Copyright © 2019 Khoa Pham. All rights reserved.
//

import UIKit

class MainTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let items = repeatElement(UIViewController(), count: 3)
        var controllers: [UIViewController] = []
        controllers.append(MainViewController(nibName: "MainViewController", bundle: nil))
        controllers.append(contentsOf: items)
        
        let titles = ["Main", "Orders", "Profile", "Checkout"]
        let icons = ["icon_home", "icon_order", "icon_profile", "icon_checkout"]
        
        var viewControllers: [UIViewController] = []
        for (index, controller) in controllers.enumerated() {
            let nvc = UINavigationController(rootViewController: controller)
            nvc.tabBarItem = UITabBarItem(title: titles[index], image: imageFromName(icons[index]), selectedImage: nil)
            viewControllers.append(nvc)
        }
        self.viewControllers = viewControllers
    }

}
