//
//  MainCell.swift
//  Entrance Test
//
//  Created by Khoa Pham on 5/16/19.
//  Copyright © 2019 Khoa Pham. All rights reserved.
//

import UIKit

private let avatarListLayoutSize: CGFloat = 100

protocol CellInterface {
    
    static var id: String { get }
    static var cellNib: UINib { get }
    
}

extension CellInterface {
    
    static var id: String {
        return String(describing: Self.self)
    }
    
    static var cellNib: UINib {
        return UINib(nibName: id, bundle: nil)
    }
    
}

class MainCell: UICollectionViewCell, CellInterface {
    
    @IBOutlet weak var photoImageView: RoundedRectUIImageView!
    @IBOutlet weak var nameListLabel: UILabel!
    @IBOutlet weak var nameGridLabel: UILabel!
    @IBOutlet weak var tagsLabel: UILabel!
    @IBOutlet weak var deliveryLabel: UILabel!
    
    @IBOutlet weak var avatarImageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var avatarImageViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var tagsLabelLeadingConstraint: NSLayoutConstraint! {
        didSet {
            initialLabelsLeadingConstraintValue = tagsLabelLeadingConstraint.constant
        }
    }
    @IBOutlet weak var nameListLabelLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var deliveryLabelLeadingConstraint: NSLayoutConstraint!
    
    fileprivate var avatarGridLayoutSize: CGFloat = 0
    fileprivate var initialLabelsLeadingConstraintValue: CGFloat = 0
    
    var food: Food? {
        didSet {
            guard let food = food else {
                return
            }
            nameListLabel.text = food.name
            tagsLabel.text = food.tags
            deliveryLabel.text = food.delivery
            photoImageView.image = imageFromName(food.photo)
            let attributedString = NSMutableAttributedString().addString("\(food.tags)\n", font: UIFont.systemFont(ofSize: 14), color: .lightGray).addString("\(food.name)\n", font: UIFont.systemFont(ofSize: 17), color: .black).addString("45-60 mins • 4 persons", font: UIFont.systemFont(ofSize: 13), color: .lightGray)
            nameGridLabel.attributedText = attributedString
        }
    }
    
    func setupGridLayoutConstraints(_ transitionProgress: CGFloat, cellWidth: CGFloat) {
        avatarImageViewHeightConstraint.constant = ceil((cellWidth - avatarListLayoutSize) * transitionProgress + avatarListLayoutSize)
        avatarImageViewWidthConstraint.constant = ceil(avatarImageViewHeightConstraint.constant)
        tagsLabelLeadingConstraint.constant = -avatarImageViewWidthConstraint.constant * transitionProgress + initialLabelsLeadingConstraintValue
        nameListLabelLeadingConstraint.constant = nameListLabelLeadingConstraint.constant
        deliveryLabelLeadingConstraint.constant = nameListLabelLeadingConstraint.constant
        nameListLabel.alpha = 1 - transitionProgress
        tagsLabel.alpha = 1 - transitionProgress
        deliveryLabel.alpha = 1 - transitionProgress
    }
    
    func setupListLayoutConstraints(_ transitionProgress: CGFloat, cellWidth: CGFloat) {
        avatarImageViewHeightConstraint.constant = ceil(avatarGridLayoutSize - (avatarGridLayoutSize - avatarListLayoutSize) * transitionProgress)
        avatarImageViewWidthConstraint.constant = avatarImageViewHeightConstraint.constant
        tagsLabelLeadingConstraint.constant = avatarImageViewWidthConstraint.constant * transitionProgress + (initialLabelsLeadingConstraintValue - avatarImageViewHeightConstraint.constant)
        nameListLabelLeadingConstraint.constant = nameListLabelLeadingConstraint.constant
        deliveryLabelLeadingConstraint.constant = nameListLabelLeadingConstraint.constant
        nameListLabel.alpha = transitionProgress
        tagsLabel.alpha = transitionProgress
        deliveryLabel.alpha = transitionProgress
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        if let attributes = layoutAttributes as? DisplaySwitchLayoutAttributes {
            if attributes.transitionProgress > 0 {
                if attributes.layoutState == .grid {
                    setupGridLayoutConstraints(attributes.transitionProgress, cellWidth: attributes.nextLayoutCellFrame.width)
                    avatarGridLayoutSize = attributes.nextLayoutCellFrame.width
                } else {
                    setupListLayoutConstraints(attributes.transitionProgress, cellWidth: attributes.nextLayoutCellFrame.width)
                }
            }
        }
    }

}
