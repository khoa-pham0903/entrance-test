//
//  SearchViewController+SearchBar.swift
//  Entrance Test
//
//  Created by Khoa Pham on 5/17/19.
//  Copyright © 2019 Khoa Pham. All rights reserved.
//

import UIKit

extension SearchViewController {
    func setupSearchBar() {
        searchController.searchBar.delegate = self
    }
}

extension SearchViewController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        delegate?.searchBarCancelButtonClicked()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self,
                                               selector: #selector(textDidChange),
                                               object: nil)
        viewModel.setState(state: .loading)
        perform(#selector(textDidChange), with: nil, afterDelay: 2.0)
    }
    
    @objc
    func textDidChange() {
        let query = searchController.searchBar.text ?? ""
        viewModel.filterContentForSearchText(query)
    }
    
}
