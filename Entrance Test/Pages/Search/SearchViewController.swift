//
//  SearchViewController.swift
//  Entrance Test
//
//  Created by Khoa Pham on 5/16/19.
//  Copyright © 2019 Khoa Pham. All rights reserved.
//

import UIKit

protocol SearchViewControllerDelegate: class {
    func searchBarCancelButtonClicked()
}

class SearchViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: SearchViewModel!
    
    let searchController: UISearchController
    
    weak var delegate: SearchViewControllerDelegate?
    
    lazy var emptyView: EmptyView = {
        let v = EmptyView.loadNib()
        return v
    }()
    
    lazy var loadingView: LoadingView = {
        let v = LoadingView.loadNib()
        return v
    }()
    
    init(searchController: UISearchController) {
        self.searchController = searchController
        super.init(nibName: "SearchViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupSearchBar()
        setupVM()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        tapGesture.cancelsTouchesInView = false
        tableView.addGestureRecognizer(tapGesture)
    }
    
    private func setupVM() {
        viewModel = SearchViewModel(searchController: searchController)
        viewModel.initData()
        viewModel.reloadData = { [weak self] in
            guard let strongSelf = self else {
                return
            }
            strongSelf.reloadData()
        }
        viewModel.setBackgroundView = { [weak self] state in
            guard let strongSelf = self else {
                return
            }
            switch state {
            case .loading:
                strongSelf.tableView.backgroundView = strongSelf.loadingView
            case .empty:
                strongSelf.tableView.backgroundView = strongSelf.emptyView
            case .populated:
                strongSelf.tableView.backgroundView = nil
            }
        }
    }
    
    func reloadData() {
        tableView.reloadData()
    }
    
    @objc
    func handleTapGesture() {
        searchController.searchBar.endEditing(true)
    }
    
}





