//
//  SearchViewController+TableView.swift
//  Entrance Test
//
//  Created by Khoa Pham on 5/17/19.
//  Copyright © 2019 Khoa Pham. All rights reserved.
//

import UIKit

extension SearchViewController {
    func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(FoodCell.self, forCellReuseIdentifier: FoodCell.id)
    }
}

extension SearchViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.item(atSection: section).rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = viewModel.item(atSection: indexPath.section)
        switch item.type {
        case .food:
            if let item = item as? SearchViewModelFoodItem, let cell = tableView.dequeueReusableCell(withIdentifier: FoodCell.id, for: indexPath) as? FoodCell {
                cell.food = item.foods[indexPath.row]
                cell.accessoryType = .disclosureIndicator
                return cell
            }
        case .video:
            if let item = item as? SearchViewModelVideoItem, let cell = tableView.dequeueReusableCell(withIdentifier: FoodCell.id, for: indexPath) as? FoodCell {
                cell.food = item.foods[indexPath.row]
                cell.accessoryType = .disclosureIndicator
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.item(atSection: section).sectionTitle
    }
}

extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = viewModel.item(atSection: indexPath.section)
        switch item.type {
        case .food:
            guard let item = item as? SearchViewModelFoodItem else {
                return
            }
            let foodItem = item.foods[indexPath.row]
            let foodDetails = FoodDetails(name: foodItem.name, photo: foodItem.photo)
            let detailsVC = FoodDetailsViewController(details: foodDetails)
            navigationController?.pushViewController(detailsVC, animated: true)
        default:
            return
        }
    }
}
