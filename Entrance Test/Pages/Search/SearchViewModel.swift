//
//  SearchViewModel.swift
//  Entrance Test
//
//  Created by Khoa Pham on 5/16/19.
//  Copyright © 2019 Khoa Pham. All rights reserved.
//

import Foundation
import UIKit

enum SearchViewModelItemType {
    case food
    case video
}

protocol SearchViewModelItem {
    var type: SearchViewModelItemType { get }
    var sectionTitle: String { get }
    var rowCount: Int { get }
}

class SearchViewModelFoodItem: SearchViewModelItem {
    var type: SearchViewModelItemType {
        return .food
    }
    
    var sectionTitle: String {
        return "FOODS"
    }
    
    var rowCount: Int {
        return foods.count
    }
    
    let foods: [Food]
    
    init(foods: [Food]) {
        self.foods = foods
    }
}

class SearchViewModelVideoItem: SearchViewModelItem {
    var type: SearchViewModelItemType {
        return .video
    }
    
    var sectionTitle: String {
        return "VIDEO FOODS"
    }
    
    var rowCount: Int {
        return foods.count
    }
    
    let foods: [Food]
    
    init(foods: [Food]) {
        self.foods = foods
    }
}
class SearchViewModel: NSObject {
    
    enum State {
        case loading
        case empty
        case populated
    }
    
    private var state: State = .loading {
        didSet {
            if state == .loading {
                items.removeAll()
            }
            setBackgroundView?(state)
            reloadData?()
        }
    }
    
    var reloadData: (() -> Void)?
    var setBackgroundView: ((State) -> Void)?
    
    private var items: [SearchViewModelItem] = []
    
    var sectionCount: Int {
        return items.count
    }
    
    func item(atSection section: Int) -> SearchViewModelItem {
        return items[section]
    }
    
    var foods: [Food] = FoodDataProvider.generateAllFoods()
    
    var filteredFoods: [Food] = []
    
    let searchController: UISearchController
    
    init(searchController: UISearchController) {
        self.searchController = searchController
    }
    
    func setState(state: State) {
        self.state = state
    }
    
    func initData() {
        items.removeAll()
        guard isFiltering() else {
            let item = SearchViewModelFoodItem(foods: foods)
            let videoItem = SearchViewModelVideoItem(foods: filterVideoFoods(foods: foods))
            items.append(item)
            items.append(videoItem)
            state = .populated
            return
        }
        guard !filteredFoods.isEmpty else {
            state = .empty
            return
        }
        let item = SearchViewModelFoodItem(foods: filteredFoods)
        let videoItem = SearchViewModelVideoItem(foods: filterVideoFoods(foods: foods))
        items.append(item)
        items.append(videoItem)
        state = .populated
    }
    
    private func filterVideoFoods(foods: [Food]) -> [Food] {
        return foods.filter { $0.hasVideo }
    }
    
    private func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    private func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filteredFoods = foods.filter({( food : Food) -> Bool in
            return food.name.lowercased().contains(searchText.lowercased())
        })
        initData()
        reloadData?()
    }
}
