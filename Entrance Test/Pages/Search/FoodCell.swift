//
//  FoodCell.swift
//  Entrance Test
//
//  Created by Khoa Pham on 5/17/19.
//  Copyright © 2019 Khoa Pham. All rights reserved.
//

import UIKit

class FoodCell: BaseTableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var food: Food? {
        didSet {
            guard let food = food else {
                return
            }
            textLabel?.text = food.name
            detailTextLabel?.text = "45-60 mins • 4 persons"
            imageView?.image = imageFromName(food.photo)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        textLabel?.frame.origin = CGPoint(x: 78, y: (textLabel?.frame.origin.y)!)
        detailTextLabel?.frame.origin = CGPoint(x: 78, y: (detailTextLabel?.frame.origin.y)!)
        imageView?.frame.origin.y = (imageView?.frame.origin.y)! + 8
        separatorInset.left = 73
        imageView?.clipsToBounds = true
        imageView?.layer.masksToBounds = true
        imageView?.contentMode = .scaleAspectFill
        imageView?.frame.size = CGSize(width: 50, height: 50)
        imageView?.layer.cornerRadius = 5
    }
}
