//
//  ExtrasCell.swift
//  Entrance Test
//
//  Created by Khoa Pham on 5/17/19.
//  Copyright © 2019 Khoa Pham. All rights reserved.
//

import UIKit

class ExtrasCell: BaseTableViewCell {
    
    @IBOutlet weak var extraLabel: UILabel!
    
    var item: FoodDetailsViewModelExtraItem? {
        didSet {
            guard let item = item else {
                return
            }
            extraLabel.text = item.extra
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        accessoryType = selected ? .checkmark : .none
    }
    
}
