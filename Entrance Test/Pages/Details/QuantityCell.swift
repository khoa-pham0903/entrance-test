//
//  QuantityCell.swift
//  Entrance Test
//
//  Created by Khoa Pham on 5/17/19.
//  Copyright © 2019 Khoa Pham. All rights reserved.
//

import UIKit

class QuantityCell: BaseTableViewCell {
    
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    
    var quantity = 0 {
        didSet {
            guard let item = item as? FoodDetailsViewModelQuantityItem else {
                return
            }
            item.quantity = quantity
            quantityLabel.text = quantity > 1 ? "\(quantity) plates" : "\(quantity) plate"
        }
    }
    
    var item: FoodDetailsViewModelItem? {
        didSet {
            guard let item = item as? FoodDetailsViewModelQuantityItem else {
                return
            }
            quantity = item.quantity
        }
    }
    
    override func setupView() {
        super.setupView()
        stepper.addTarget(self, action: #selector(valueChanged), for: .valueChanged)
    }
    
    @objc
    func valueChanged(sender: UIStepper) {
        quantity = Int(sender.value)
    }
}
