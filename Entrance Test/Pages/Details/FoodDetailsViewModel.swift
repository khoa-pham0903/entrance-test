//
//  FoodDetailsViewModel.swift
//  Entrance Test
//
//  Created by Khoa Pham on 5/17/19.
//  Copyright © 2019 Khoa Pham. All rights reserved.
//

import Foundation

enum FoodDetailsViewModelItemType {
    case details
    case extras
    case quantity
}

protocol FoodDetailsViewModelItem {
    var type: FoodDetailsViewModelItemType { get }
    var sectionTitle: String { get }
    var rowCount: Int { get }
}

extension FoodDetailsViewModelItem {
    var rowCount: Int {
        return 1
    }
}

class FoodDetailsViewModelDetailsItem: FoodDetailsViewModelItem {
    var type: FoodDetailsViewModelItemType {
        return .details
    }
    
    var sectionTitle: String {
        return ""
    }
    
    var name: String {
        return details.name
    }
    
    var photo: String {
        return details.photo
    }
    
    private let details: FoodDetails
    
    init(details: FoodDetails) {
        self.details = details
    }
}

class FoodDetailsViewModelExtraItem {
    let extra: String
    var isSelected = false
    
    init(extra: String) {
        self.extra = extra
    }
}

class FoodDetailsViewModelExtrasItem: FoodDetailsViewModelItem {
    var type: FoodDetailsViewModelItemType {
        return .extras
    }
    
    var sectionTitle: String {
        return "EXTRAS"
    }
    
    var rowCount: Int {
        return extraItems.count
    }
    
    var selectedExtraItems: [FoodDetailsViewModelExtraItem] {
        return extraItems.filter { return $0.isSelected }
    }
    
    var selectedExtras: String = "" {
        didSet {
            details.selectedExtras = selectedExtras
        }
    }
    
    private let details: FoodDetails
    let extraItems: [FoodDetailsViewModelExtraItem]
    
    init(details: FoodDetails, extraItems: [FoodDetailsViewModelExtraItem]) {
        self.details = details
        self.extraItems = extraItems
    }
}

class FoodDetailsViewModelQuantityItem: FoodDetailsViewModelItem {
    var type: FoodDetailsViewModelItemType {
        return .quantity
    }
    
    var sectionTitle: String {
        return "QUANTITY"
    }
    
    private let details: FoodDetails
    
    var quantity: Int  {
        get {
            return details.quantity
        }
        set {
            details.quantity = newValue
        }
    }
    
    init(details: FoodDetails) {
        self.details = details
    }
}

import UIKit

class FoodDetailsViewModel: NSObject {
    
    private var items: [FoodDetailsViewModelItem] = []
    
    var reloadData: (() -> Void)?
    
    private let details: FoodDetails
    
    var sectionCount: Int {
        return items.count
    }
    
    func item(atSection section: Int) -> FoodDetailsViewModelItem {
        return items[section]
    }
    
    init(details: FoodDetails) {
        self.details = details
    }
    
    func initData() {
        let detailsItem = FoodDetailsViewModelDetailsItem(details: details)
        let extraItems = details.extras.map { FoodDetailsViewModelExtraItem(extra: $0) }
        let extrasItem = FoodDetailsViewModelExtrasItem(details: details, extraItems: extraItems)
        let quantityItem = FoodDetailsViewModelQuantityItem(details: details)
        items.append(detailsItem)
        items.append(extrasItem)
        items.append(quantityItem)
        reloadData?()
    }
}
