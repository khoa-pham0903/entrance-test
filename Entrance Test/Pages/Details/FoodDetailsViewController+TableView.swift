//
//  FoodDetailsViewController+TableView.swift
//  Entrance Test
//
//  Created by Khoa Pham on 5/17/19.
//  Copyright © 2019 Khoa Pham. All rights reserved.
//

import UIKit

extension FoodDetailsViewController {
    func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsMultipleSelection = true
        tableView.register(FoodDetailCell.cellNib, forCellReuseIdentifier: FoodDetailCell.id)
        tableView.register(ExtrasCell.cellNib, forCellReuseIdentifier: ExtrasCell.id)
        tableView.register(QuantityCell.cellNib, forCellReuseIdentifier: QuantityCell.id)
    }
}

extension FoodDetailsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.item(atSection: section).rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = viewModel.item(atSection: indexPath.section)
        switch item.type {
        case .details:
            if let cell = tableView.dequeueReusableCell(withIdentifier: FoodDetailCell.id, for: indexPath) as? FoodDetailCell {
                cell.item = item
                return cell
            }
        case .extras:
            if let item = item as? FoodDetailsViewModelExtrasItem, let cell = tableView.dequeueReusableCell(withIdentifier: ExtrasCell.id, for: indexPath) as? ExtrasCell {
                cell.item = item.extraItems[indexPath.row]
                return cell
            }
        case .quantity:
            if let cell = tableView.dequeueReusableCell(withIdentifier: QuantityCell.id, for: indexPath) as? QuantityCell {
                cell.item = item
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.item(atSection: section).sectionTitle
    }
}

extension FoodDetailsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let item = viewModel.item(atSection: section)
        switch item.type {
        case .details:
            return CGFloat.leastNormalMagnitude
        default:
            return 44
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = viewModel.item(atSection: indexPath.section)
        switch item.type {
        case .extras:
            guard let item = item as? FoodDetailsViewModelExtrasItem else {
                return
            }
            item.extraItems[indexPath.row].isSelected = true
            item.selectedExtras = "\(item.selectedExtraItems.map { $0.extra })"
        default:
            return
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let item = viewModel.item(atSection: indexPath.section)
        switch item.type {
        case .extras:
            guard let item = item as? FoodDetailsViewModelExtrasItem else {
                return
            }
            item.extraItems[indexPath.row].isSelected = false
            item.selectedExtras = "\(item.selectedExtraItems.map { $0.extra })"
        default:
            return
        }
    }
}
