//
//  FoodDetailCell.swift
//  Entrance Test
//
//  Created by Khoa Pham on 5/17/19.
//  Copyright © 2019 Khoa Pham. All rights reserved.
//

import UIKit

class FoodDetailCell: BaseTableViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var item: FoodDetailsViewModelItem? {
        didSet {
            guard let item = item as? FoodDetailsViewModelDetailsItem else {
                return
            }
            nameLabel.text = item.name
            photoImageView.image = imageFromName(item.photo)
        }
    }
    
}
