//
//  FoodDetailsViewController.swift
//  Entrance Test
//
//  Created by Khoa Pham on 5/17/19.
//  Copyright © 2019 Khoa Pham. All rights reserved.
//

import UIKit

class FoodDetailsViewController: UIViewController ,AlertDisplayer {

    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: FoodDetailsViewModel!
    
    private let details: FoodDetails
    
    init(details: FoodDetails) {
        self.details = details
        super.init(nibName: "SearchViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setupTableView()
        setupVM()
    }
    
    private func setupNavBar() {
        Helper.changeBackButtonTitle(self)
        let barButton = UIBarButtonItem(title: "Order", style: .plain, target: self, action: #selector(handleOrder))
        navigationItem.rightBarButtonItem = barButton
    }
    
    private func setupVM() {
        viewModel = FoodDetailsViewModel(details: details)
        viewModel.initData()
        viewModel.reloadData = { [weak self] in
            guard let strongSelf = self else {
                return
            }
            strongSelf.reloadData()
        }
    }
    
    private func reloadData() {
        tableView.reloadData()
    }
    
    @objc
    func handleOrder() {
        let title = "Order"
        let message = "Name: \(details.name) \n Quantity: \(details.quantity) \n Extras: \(details.selectedExtras)"
        let action = UIAlertAction(title: "OK", style: .default)
        displayAlert(with: title , message: message, style: .alert, actions: [action])
    }

}




